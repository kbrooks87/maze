/* Thanks to Howard Post for explaining a lot of this code
to me and helping me to build this */

const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWW W WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
]


const displayBoard = function(map) {
    let table = document.createElement("table")
    table.style.width = "660px"
    table.style.height = "420px"
    for (let r = 0; r < map.length; r++) {
        let box = map[r]
        let row = document.createElement("div")
        row.style.display = "flex"
        row.className = "row"
        row.id = "row" + r
        for (let col = 0; col < box.length; col++) {
            let column = document.createElement("div")
            column.style.width = "30px"
            column.style.height = "30px"
            column.style.display = "inline-block"
                //column.style.margin = "collapse"
                //column.style.padding = "collapse"
            column.style.position = "relative"
            if (box[col] === "W") {
                // let wText = document.createTextNode("W")
                //column.appendChild(wText)
                column.style.backgroundImage = "url(img/space.jpeg)"
                column.setAttribute("data-row", r)
                column.setAttribute("data-column", col)
                column.setAttribute("data-wall", "y")
            } else if (box[col] === " ") {
                //let pText = document.createTextNode(" ")
                //column.appendChild(pText)
                column.style.backgroundImage = "url(img/floorSpace.jpeg)"
                column.setAttribute("data-row", r)
                column.setAttribute("data-column", col)
                column.setAttribute("data-wall", "n")
            } else if (box[col] === "S") {
                // let sText = document.createTextNode("S")
                let player = document.createElement("div")
                player.style.width = "30px"
                player.style.height = "30px"
                player.style.position = "absolute"
                player.style.display = "inline-block"
                player.id = "character"
                let pImg = document.createElement("img")
                pImg.style.width = "30px"
                pImg.style.height = "30px"
                pImg.src = "img/g00.jpeg"
                player.appendChild(pImg)
                    //player.style.backgroundImage = "url(img/g00.jpeg)"
                let pText = document.createTextNode("P")
                    //player.appendChild(pText)
                    //column.appendChild(sText)
                column.appendChild(player)
                column.setAttribute("data-row", r)
                column.setAttribute("data-column", col)
                column.setAttribute("data-wall", "n")
            } else if (box[col] === "F") {
                //let finish = document.createTextNode("F")
                //column.appendChild(finish)
                column.style.backgroundImage = "url(img/finish.jpeg)"
                column.setAttribute("data-row", r)
                column.setAttribute("data-column", col)
                column.setAttribute("data-wall", "F")
            }
            column.className = "column"
            column.id = "r" + r + "c" + col
            row.appendChild(column)
        }
        table.appendChild(row)
    }
    document.body.appendChild(table)
    table.id = "table"
}


const initializeGame = function() {
    displayBoard(map)
}

initializeGame()

const move = function(evt) {
    let gundam = document.getElementById("character")
    let parent = gundam.parentElement
    let pRow = parseInt(parent.dataset.row)
    let pColumn = parseInt(parent.dataset.column)
    if (evt.code === "ArrowDown") {
        let nDiv = document.querySelector("div[data-row ='" + (pRow + 1) + "'][data-column = '" + pColumn + "']")
        if (nDiv.dataset.wall === "y") {
            alert("BLACKHOLE ALERT!!")
        } else if (nDiv === "F") {
            alert("You WON!!")
        } else {
            nDiv.appendChild(gundam)
        }
    } else if (evt.code === "ArrowUp") {
        let nDiv = document.querySelector("div[data-row = '" + (pRow - 1) + "'][data-column = '" + pColumn + "']")
        if (nDiv.dataset.wall === "y") {
            alert("BLACKHOLE ALERT!!")
        } else if (nDiv === "F") {
            alert("You WON!!")
        } else {
            nDiv.appendChild(gundam)
        }
    } else if (evt.code === "ArrowRight") {
        let nDiv = document.querySelector("div[data-row = '" + pRow + "'][data-column = '" + (pColumn + 1) + "']")
        if (nDiv === null) {
            alert("End of Universe")
        } else if (nDiv.dataset.wall === "y") {
            alert("BLACKHOLE ALERT!!")
        } else if (nDiv.dataset.wall === "F") {
            nDiv.appendChild(gundam)
            alert("Zaku Destroyed!!")
        } else {
            nDiv.appendChild(gundam)
        }
    } else if (evt.code === "ArrowLeft") {
        let nDiv = document.querySelector("div[data-row = '" + pRow + "'][data-column = '" + (pColumn - 1) + "']")
        if (nDiv.dataset.wall === "y") {
            alert("BLACKHOLE ALERT!!")
        } else if (nDiv === "F") {
            alert("You WON!!")
        } else {
            nDiv.appendChild(gundam)
        }
    }
}

document.addEventListener('keydown', move)